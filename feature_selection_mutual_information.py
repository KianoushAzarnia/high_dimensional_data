import pandas as pd
import numpy as np
from sklearn.feature_selection import mutual_info_classif, SelectPercentile
from domain import models, datapath_list, dataset_list, parameters

datasets_selected_features = dict()
def_percent = 5

for x_path, y_path in datapath_list:
    X = dataset_list[(x_path, y_path)][0]
    Y = dataset_list[(x_path, y_path)][1]
    selected_features = SelectPercentile(mutual_info_classif, percentile=def_percent).fit_transform(X, Y)
    datasets_selected_features.update({x_path : selected_features})
    print(x_path ,datasets_selected_features[x_path].shape)

print('feature selection with mutual info finished.')
