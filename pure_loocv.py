import pandas as pd
import numpy as np
#import seaborn as sns
#import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix #roc_auc_score, accuracy_score, precision_score, recall_score, f1_score,
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier #, ExtraTreesClassifier, VotingClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
#from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.metrics import precision_recall_fscore_support, mean_squared_error as mse
# from sklearn.utils.multiclass import unique_labels
from sklearn import preprocessing
#from sklearn.model_selection import cross_validate
from sklearn.model_selection import train_test_split, GridSearchCV, LeaveOneOut
from domain import models, datapath_list, dataset_list

results = dict()
scores = ['accuracy', 'precision', 'recall', 'f1']


def leave_one_out(classifier, X, y):
    loo = LeaveOneOut()
    Y_pred = []
    Y_true = []
    i = 0
    for train_index, test_index in loo.split(X):
        i = i + 1
        X_train, X_test = X.iloc[train_index], X.iloc[test_index]
        y_train, y_test = y.iloc[train_index], y.iloc[test_index]
        Y_true.append(y_test) #y_test[0].tolist()
        model = classifier.fit(X_train, y_train) #y_train.ravel()
        y_pred = model.predict(X_test)
        Y_pred.append(y_pred) #y_pred[0]

    confusion_matrix_loo = confusion_matrix(Y_true, Y_pred)
    print('\nconfusion matrix = ', confusion_matrix_loo.tolist())
    print(i)


for model_name in models:
    print(model_name)
    for x_path, y_path in datapath_list:
        print('---> dataset: ', x_path)
        X = dataset_list[(x_path, y_path)][0]
        Y = dataset_list[(x_path, y_path)][1]
        leave_one_out(models[model_name], X, Y)
