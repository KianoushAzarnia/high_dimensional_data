import pandas as pd
import numpy as np
#import seaborn as sns
#import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix #roc_auc_score, accuracy_score, precision_score, recall_score, f1_score,
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier #, ExtraTreesClassifier, VotingClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
#from sklearn.cluster import KMeans
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import AdaBoostClassifier
from zipfile import ZipFile
from sklearn.metrics import precision_recall_fscore_support, mean_squared_error as mse
# from sklearn.utils.multiclass import unique_labels
from sklearn import preprocessing
#from sklearn.model_selection import cross_validate
from sklearn.model_selection import train_test_split, GridSearchCV, LeaveOneOut
#from sklearn.feature_selection import SelectFromModel
#from sklearn.feature_selection import RFECV

import warnings
warnings.filterwarnings('ignore')


with ZipFile('shipp_lymphoma.zip', 'r') as zip_object:
    zip_object.extractall()


datapath_list = [('colon.csv', 'colon_indicator.csv'),
                 ('gravier_breast_cancer.csv', 'gravier_breast_cancer_indicator.csv'),
                 ('Leukemia.csv', 'Leukemia_indicator.csv'),
                 ('shipp_lymphoma.csv', 'shipp_lymphoma_indicator.csv'),
                ]

models = {
          'knn': KNeighborsClassifier(),
          'gnb': GaussianNB(),
          'mlp': MLPClassifier(),
          'adaboost': AdaBoostClassifier(),
          'lr': LogisticRegression(),
          'dtree': DecisionTreeClassifier(),
          'svm': SVC(),
          'random_forest': RandomForestClassifier(),
          'lda': LinearDiscriminantAnalysis(solver='eigen',shrinkage='auto'),
}

dataset_list = dict()

parameters = {
    'lr': {},
    'dtree': {},
    'svm': {'C': [0.001, 0.01, 0.1, 1, 10],
            'gamma': [0.001, 0.01, 0.1, 1, 'auto'],
            'kernel': ['linear', 'poly', 'rbf', 'sigmoid'],
            'decision_function_shape': ['ovo', 'ovr'],
            'shrinking': [True, False],
            },
    'random_forest': {
        'n_estimators': [200, 700],
        'max_features': ['auto', 'sqrt', 'log2']
    },
    'knn': {'n_neighbors': [3, 5],
            'weights': ['uniform', 'distance'],
            'metric': ['braycurtis', 'canberra', 'euclidean', 'minkowski'], #'correlation', 'cosine',
                #['euclidean', 'minkowski', 'manhattan', 'p', 'l1', 'l2', 'cityblock', 'chebyshev', 'infinity', ],
            'algorithm': ['auto', 'ball_tree'], #'kd_tree'
            },
    'gnb': {},
    'lda': {},
    'adaboost': {#'base_estimator__criterion': ['gini', 'entropy'],
        #'base_estimator__splitter': ['best', 'random'],
        'n_estimators': [1, 2, 50, 100],
        'learning_rate': [0.01, 0.05, 0.1, 0.3, 1],
        'algorithm': ['SAMME', 'SAMME.R'],
    },
    'mlp': {}
    # {
    #     'hidden_layer_sizes': [(50, 50, 50), (50, 100, 50), (100,)],
    #     'activation': ['tanh', 'relu', 'identity', 'logistic'],
    #     'solver': ['sgd', 'adam', 'lbfgs'],
    #     'alpha': [0.0001, 0.05],
    #     'learning_rate': ['constant', 'adaptive'],
    # }
}

for x_path, y_path in datapath_list:
    X = pd.read_csv(x_path, header=None).astype('float16')
    Y = pd.read_csv(y_path, header=None).astype('float16')
    dataset_list.update({(x_path, y_path): (X, Y)})

print("domain finished")
