import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix #roc_auc_score, accuracy_score, precision_score, recall_score, f1_score,
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier #, ExtraTreesClassifier, VotingClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import train_test_split, GridSearchCV, LeaveOneOut
import warnings
from domain import models, datapath_list, dataset_list, parameters
from feature_selection_mutual_information import datasets_selected_features

warnings.filterwarnings('ignore')

loocv = LeaveOneOut()
param_grid = {}
grid_mutual_results = dict()

for model_name in models:
    print(model_name)
    for x_path, y_path in datapath_list:
        print('---> dataset: ', x_path)
        X = datasets_selected_features[x_path]
        Y = dataset_list[(x_path, y_path)][1]

        clf = GridSearchCV(models[model_name], parameters[model_name], scoring='accuracy', cv=loocv)

        clf.fit(X, Y)

        Y_pred = clf.best_estimator_.predict(X)

        confusion_matrix_loo = confusion_matrix(Y, Y_pred)

        grid_mutual_results.update({(model_name, x_path): confusion_matrix_loo})
        print('\nconfusion matrix = ', confusion_matrix_loo.tolist())
        print('-----------------------------------------------------------------------\n')

print(grid_mutual_results)