import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix #roc_auc_score, accuracy_score, precision_score, recall_score, f1_score,
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC, LinearSVC
from sklearn.ensemble import RandomForestClassifier #, ExtraTreesClassifier, VotingClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier, VALID_METRICS
from sklearn.naive_bayes import GaussianNB, BernoulliNB
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import AdaBoostClassifier
from sklearn.model_selection import train_test_split, GridSearchCV, LeaveOneOut
import warnings
from domain import models, datapath_list, dataset_list, parameters
from feature_selection_mutual_information import  datasets_selected_features

warnings.filterwarnings('ignore')

# print(X.shape)
# print(type(X.shape))

# np_train_sets = dict()

# for x_path, y_path in datapath_list:
#     row_x, row_y = 0, 0
#     X = dataset_list[(x_path, y_path)][0]
#     print(x_path, ' min = ', X.min().min())
#     print(x_path, ' max = ', X.max().max())
#     Y = dataset_list[(x_path, y_path)][1]
#     train_x = np.empty([X.shape[0], X.shape[1]])
#     train_y = np.empty([Y.shape[0], Y.shape[1]])
#     for line in open(x_path):
#         train_x[row_x] = np.fromstring(line, sep=",")
#         row_x += 1
#     for line in open(y_path):
#         train_y[row_y] = np.fromstring(line, sep=",")
#         row_y += 1
#     np_train_sets.update({(x_path, y_path): (train_x, train_y)})

# print(VALID_METRICS['ball_tree'])

for x_path, y_path in datapath_list:
    print(x_path, dataset_list[(x_path, y_path)][0].shape)
    print(y_path, dataset_list[(x_path, y_path)][1].shape)
